from django.urls import path

from . import views

app_name = 'phonebook'

urlpatterns = [
    path('', views.PersonList.as_view(), name='person_list'),
    path('new', views.PersonCreate.as_view(), name='person_new'),
    path('edit/<int:pk>', views.PersonUpdate.as_view(), name='person_edit'),
    path('delete/<int:pk>', views.PersonDelete.as_view(), name='person_delete'),
    path('new_email/<int:person_id>',
         views.EmailCreate.as_view(), name='email_new'),
    path('new_phone_number/<int:person_id>',
         views.PhoneNumberCreate.as_view(), name='phone_number_new')
]
