from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.forms import ModelForm
from django.shortcuts import get_object_or_404
from django.db.models import Q

from .forms import EmailForm, PhoneNumberForm
from phonebook.models import Person, Email, PhoneNumber
# Create your views here.


def index(request):
    return HttpResponse('This is a phone book app.')


class PersonList(ListView):
    model = Person
    template_name = '\phonebook\person_list.html'

    def get_context_data(self, **kwargs):
        context = super(PersonList, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            object_list = Person.objects.filter(
                Q(surname__icontains=query) | Q(name__icontains=query) |
                Q(emails__email__icontains=query) |
                Q(phonenumbers__phone_number__icontains=query)
            ).distinct()
        else:
            object_list = Person.objects.all()

        return object_list


class PersonCreate(CreateView):
    model = Person
    fields = ['name', 'surname']
    success_url = reverse_lazy('phonebook:person_list')


class PersonUpdate(UpdateView):
    model = Person
    fields = ['name', 'surname']
    success_url = reverse_lazy('phonebook:person_list')


class PersonDelete(DeleteView):
    model = Person
    success_url = reverse_lazy('phonebook:person_list')


class EmailCreate(CreateView):
    template_name = 'phonebook/email_form.html'
    form_class = EmailForm

    def form_valid(self, form):
        email = form.save(commit=False)
        person_id = form.data['person_id']
        person = get_object_or_404(Person, id=person_id)
        email.person = person
        return super(EmailCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        print(kwargs)
        context = super(EmailCreate, self).get_context_data(**kwargs)
        context['a_id'] = self.kwargs['person_id']
        return context

    success_url = reverse_lazy('phonebook:person_list')


class PhoneNumberCreate(CreateView):
    template_name = 'phonebook/phonenumber_form.html'
    form_class = PhoneNumberForm

    def form_valid(self, form):
        phone_number = form.save(commit=False)
        person_id = form.data['person_id']
        person = get_object_or_404(Person, id=person_id)
        phone_number.person = person
        return super(PhoneNumberCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        print(kwargs)
        context = super(PhoneNumberCreate, self).get_context_data(**kwargs)
        context['a_id'] = self.kwargs['person_id']
        return context

    success_url = reverse_lazy('phonebook:person_list')
