from django import forms
from .models import Email, PhoneNumber


class EmailForm(forms.ModelForm):
    class Meta:
        model = Email
        fields = '__all__'

        def __init__(self, *args, **kwargs):
            self.fields["person_id"] = forms.CharField(
                widget=forms.HiddenInput())
            super(EmailForm, self).__init__(self, *args, **kwargs)


class PhoneNumberForm(forms.ModelForm):
    class Meta:
        model = PhoneNumber
        fields = '__all__'

        def __init__(self, *args, **kwargs):
            self.fields["person_id"] = forms.CharField(
                widget=forms.HiddenInput())
            super(PhoneNumberForm, self).__init__(self, *args, **kwargs)
